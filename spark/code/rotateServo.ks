DECLARE PARAMETER somePartTag, someRotationTarget.

SET someSuccessStatus TO FALSE.
SET someServo TO LIST ().

FOR someServoInternal IN servosList {
    IF someServoInternal[partTag] = somePartTag {
        SET someSuccessStatus TO someServoInternal[successStatus].
		SET someServo TO someServoInternal.
    }
}

IF someSuccessStatus <> TRUE {
	FOR someParts IN SHIP:PARTSTAGGED(somePartTag) {
	
		SET someRotation TO someParts:GETMODULE("MuMechToggle"):GETFIELD("Rotation:").
		SET someRotation TO ROUND(someRotation).
		
		SET someMinRotate TO someParts:GETMODULE("MuMechToggle"):GETFIELD("Min Rotate").
		SET someMaxRotate TO someParts:GETMODULE("MuMechToggle"):GETFIELD("Max Rotate").
		
		IF someRotation = someRotationTarget {
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move -", FALSE).
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move +", FALSE).
				someParts:GETMODULE("MuMechToggle"):SETFIELD("Min Rotate", someMinRotate).
				someParts:GETMODULE("MuMechToggle"):SETFIELD("Max Rotate", someMaxRotate).
				IF someServo[partTag] = somePartTag {
					SET someSuccessStatus TO TRUE.
					SET someServo[successStatus] TO someSuccessStatus.
				}
		}.
		ELSE {
			IF someRotation  < someRotationTarget {
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move -", TRUE).
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move +", FALSE).
				someParts:GETMODULE("MuMechToggle"):SETFIELD("Max Rotate", someRotationTarget).
			}.
			ELSE {
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move -", FALSE).
				someParts:GETMODULE("MuMechToggle"):DOACTION("Move +", TRUE).
				someParts:GETMODULE("MuMechToggle"):SETFIELD("Min Rotate", someRotationTarget).
			}.
		}.
	}.
}.

IF debug = TRUE {
	PRINT "servo: " + somePartTag + " successStatus: " + someSuccessStatus.
}