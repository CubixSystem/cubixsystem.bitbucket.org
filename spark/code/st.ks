SET debug TO TRUE.

SET programStatus TO "LOADING".
print "***STATUS:" + programStatus + "***" .

SET partTag TO 0.
SET minRotate TO 1.
SET maxRotate TO 2.
SET rotateSpeed TO 3.
SET successStatus TO 4.
SET curRotate TO 5.

SET servoPrototype TO LIST  ().
servoPrototype:INSERT(partTag, "NONE").
servoPrototype:INSERT(minRotate, 0).
servoPrototype:INSERT(maxRotate, 0).
servoPrototype:INSERT(rotateSpeed, 0).
servoPrototype:INSERT(successStatus, FALSE).
servoPrototype:INSERT(curRotate, 0).

SET servosList TO LIST  ().

SET servo1J1 TO servoPrototype:COPY.
servosList:ADD(servo1J1).

SET servo1J2 TO servoPrototype:COPY.
servosList:ADD(servo1J2).

SET servo1J3 TO servoPrototype:COPY.
servosList:ADD(servo1J3).

SET servo1J4 TO servoPrototype:COPY.
servosList:ADD(servo1J4).

SET servo1J5 TO servoPrototype:COPY.
servosList:ADD(servo1J5).

SET servo1J6 TO servoPrototype:COPY.
servosList:ADD(servo1J6).

SET servo1J7 TO servoPrototype:COPY.
servosList:ADD(servo1J7).

SET servo1J8 TO servoPrototype:COPY.
servosList:ADD(servo1J8).

SET servo1J9 TO servoPrototype:COPY.
SET servo1J9[partTag] TO "1J9".
SET servo1J9[minRotate] TO 0.
SET servo1J9[maxRotate] TO 90.
SET servo1J9[rotateSpeed] TO 3.
SET servo1J9[successStatus] TO FALSE.
FOR someParts IN SHIP:PARTSTAGGED(servo1J9[partTag]) {
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Coarse Speed", servo1J9[rotateSpeed]).
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Min Rotate", servo1J9[minRotate]).
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Max Rotate", servo1J9[maxRotate]).
	someParts:GETMODULE("MuMechToggle"):DOEVENT("Rotate Limits Off").
	SET someRotation TO someParts:GETMODULE("MuMechToggle"):GETFIELD("Rotation:").
}.
SET someRotation TO ROUND(someRotation).
SET servo1J9[curRotate] TO someRotation.
servosList:ADD(servo1J9).
PRINT servo1J9:DUMP.

SET servo1J10 TO servoPrototype:COPY.
SET servo1J10[partTag] TO "1J10".
SET servo1J10[minRotate] TO 0.
SET servo1J10[maxRotate] TO 180.
SET servo1J10[rotateSpeed] TO 3.
SET servo1J10[successStatus] TO FALSE.
FOR someParts IN SHIP:PARTSTAGGED(servo1J10[partTag]) {
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Coarse Speed", servo1J10[rotateSpeed]).
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Min Rotate", servo1J10[minRotate]).
	someParts:GETMODULE("MuMechToggle"):SETFIELD("Max Rotate", servo1J10[maxRotate]).
	SET someRotation TO someParts:GETMODULE("MuMechToggle"):GETFIELD("Rotation:").
}.
SET someRotation TO ROUND(someRotation).
SET servo1J10[curRotate] TO someRotation.
servosList:ADD(servo1J10).

SET WheelTransformState1J TO "Wheel".

SET programStatus TO "READY".
print "***STATUS:" + programStatus + "***" .