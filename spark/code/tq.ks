//SET reachTargetX TO 0.5.
//SET reachTargetY TO 0.5.

//RUN kn.

SET programStatus TO "OPERATIONAL".
print "***STATUS:" + programStatus + "***" .

UNTIL programStatus <> "OPERATIONAL" {

	IF (servo1J9[successStatus] <> TRUE) {
		RUN  rotateServo("1J9", 0).
	}.

	IF (servo1J10[successStatus] <> TRUE) {
		RUN  rotateServo("1J10", 0).
	}.
	
	IF ((servo1J9[successStatus] = TRUE) AND (servo1J10[successStatus]  = TRUE))  {
		SET programStatus TO "END".
		print "***STATUS:" + programStatus + "***" .
		
		SET servo1J9[successStatus] TO FALSE.
		SET servo1J10[successStatus] TO FALSE.
	}.
	
}.