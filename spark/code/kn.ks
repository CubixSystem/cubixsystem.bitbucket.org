CLEARSCREEN.

SET firstSegmentLength TO 0.3.  
SET secondSegmentLength TO 0.5. 

SET d TO SQRT(reachTargetX ^ 2 + reachTargetY ^ 2).
SET Pi TO 3.1415.

IF (d <> 0 and d <= firstSegmentLength + secondSegmentLength and reachTargetY > 0 and reachTargetX > 0) {
	
	SET R1 TO firstSegmentLength.
	SET R2 TO secondSegmentLength.

	SET b TO (R2 ^ 2 - R1 ^ 2 + d ^ 2) / (2 * d).
	SET a TO d - b.

	SET h TO SQRT(R2 ^ 2 - b ^ 2).

	SET P1x TO 0.
	SET P1y TO 0.

	SET P2x TO reachTargetX.
	SET P2y TO reachTargetY.

	SET P0x TO P1x + a / d * (P2x-P1x).
	SET P0y TO P1y + a / d * (P2y-P1y).

	SET P3x TO P0x + (P2y-P1y) / d * h.
	SET P3y TO P0y - (P2x-P1x) / d * h.

	SET P4x TO P0x - (P2y-P1y) / d * h.
	SET P4y TO P0y + (P2x-P1x) / d * h.

	SET q1 TO ARCCOS((P3x * R1 + P3y * 0 ) / (R1 * R1)) .
	PRINT "q1: " + q1.
	SET joint2Angle TO ROUND(q1).

	SET p1x TO (P1x - P4x).
	SET p1y TO (P1y-  P4y).

	SET p2x TO (P2x - P4x).
	SET p2y TO (P2y - P4y).

	SET q2 TO ARCCOS((p1x * p2x + p1y * p2y) / (R1 * R2)).
	SET joint3Angle TO ROUND(q2).
	
	PRINT "joint2Angle: " + joint2Angle.
	PRINT "joint3Angle: " + joint3Angle.
}.
ELSE {
	PRINT "Error!".
}.

